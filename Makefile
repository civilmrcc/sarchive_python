include .env-dev

DEV_COMPOSE_FILE = docker-compose-dev.yml

# ========= DEV ENVIRONMENT ============================================================================================
# For local test running from IDE or terminal. Only creates S3 and Postgres DB. Dagit needs to be started from the
# terminal with make dagit. Alternativ can the job be called directly from the IDE.
dev:
	docker-compose --file $(DEV_COMPOSE_FILE) --profile dev --env-file .env-dev up --build --detach --remove-orphans
	mc alias set minio http://127.0.0.1:9000 minioadmin minioadmin --api S3v4
	mc mb minio/sarchive

clean-dev:
	docker-compose --file $(DEV_COMPOSE_FILE) --profile dev  --env-file .env-dev down -v --remove-orphans

stop-dev:
	docker-compose --file $(DEV_COMPOSE_FILE) --profile dev  --env-file .env-dev stop

reset-dev: | clean-dev dev

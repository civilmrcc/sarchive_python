import pyspark
from delta import configure_spark_with_delta_pip
from pyspark.sql import SparkSession

from sarchive_python.infrastructure.s3 import S3Connection


class SparkConnection:
    def __init__(self, master: str, s3: S3Connection):
        conf = pyspark.SparkConf()
        conf.setMaster(master).setAppName("sarchive_dagster")

        # S3
        conf.set(
            "spark.delta.logStore.class",
            "org.apache.spark.sql.delta.storage.S3SingleDriverLogStore",
        )
        conf.set("spark.hadoop.fs.path.style.access", "true")
        conf.set("spark.hadoop.fs.s3a.access.key", s3.access_key)
        conf.set("spark.hadoop.fs.s3a.secret.key", s3.secret_key)
        conf.set("spark.hadoop.fs.s3a.endpoint", s3.base_url)
        conf.set("spark.hadoop.fs.s3a.connection.ssl.enabled ", "true")
        conf.set("spark.hadoop.fs.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem")

        # Delta Lake
        conf.set("spark.sql.extensions ", "io.delta.sql.DeltaSparkSessionExtension")
        conf.set(
            "spark.sql.catalog.spark_catalog",
            "org.apache.spark.sql.delta.catalog.DeltaCatalog",
        )
        conf.set("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0")

        self._conf = conf

    @property
    def spark_session(self):
        builder = SparkSession.builder.config(conf=self._conf)
        spark = configure_spark_with_delta_pip(builder).getOrCreate()
        return spark


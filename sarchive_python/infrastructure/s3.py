import boto3


class S3Connection:
    def __init__(
        self,
        region: str,
        base_url: str,
        bucket_url: str,
        access_key: str,
        secret_key: str,
    ):
        self.region = region
        self.base_url = base_url
        self.bucket_url = bucket_url
        self.access_key = access_key
        self.secret_key = secret_key

    @property
    def boto3_resource(self):
        return boto3.resource(
            "s3",
            region_name=self.region,
            endpoint_url=self.bucket_url,
            aws_access_key_id=self.access_key,
            aws_secret_access_key=self.secret_key,
        )

import sys
from dotenv import dotenv_values

from sarchive_python.infrastructure.s3 import S3Connection
from sarchive_python.infrastructure.spark import SparkConnection


class Sarchive:
    SARCHIVE_ITEMS_TABLE = "s3a://sarchive/historical_db/items"
    SARCHIVE_CASES_TABLE = "s3a://sarchive/historical_db/cases"
    SARCHIVE_PATHS_TABLE = "s3a://sarchive/historical_db/paths"
    SARCHIVE_POSITIONS_TABLE = "s3a://sarchive/historical_db/positions"

    def __init__(self):
        env_file = ".env-dev"

        if len(sys.argv) > 1 and sys.argv[1] == "--prod":
            env_file = ".env-prod"

        self.config = dotenv_values(env_file)

        self.s3_connection = S3Connection(
            region=self.config["S3_REGION"],
            base_url=self.config["S3_BASE_URL"],
            bucket_url=self.config["S3_BUCKET_URL"],
            access_key=self.config["S3_BUCKET_KEY"],
            secret_key=self.config["S3_SECRET_KEY"],
        )

        self.spark_connection = SparkConnection(
            master=self.config["SPARK_MASTER"],
            s3=self.s3_connection
        )

    @property
    def s3(self):
        return self.s3_connection.boto3_resource

    @property
    def spark(self):
        return self.spark_connection.spark_session

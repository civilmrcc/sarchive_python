from sarchive_python.infrastructure.sarchive import Sarchive

# Initialize Spark
sarchive = Sarchive()
spark = sarchive.spark

# Read cases from SARchive
cases_sdf = spark.read.format("delta").load(sarchive.SARCHIVE_CASES_TABLE)
cases_sdf.printSchema()
# cases_sdf.show()

# Convert Cases to Pandas
cases_pdf = cases_sdf.toPandas()
print(cases_pdf)
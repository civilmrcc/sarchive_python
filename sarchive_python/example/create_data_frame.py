from delta import DeltaTable
from pyspark.sql.types import (
    StructType,
    StructField,
    StringType,
    IntegerType,
)
from pyspark.sql.utils import AnalysisException

from sarchive_python.infrastructure.sarchive import Sarchive

# Initialize Spark
sarchive = Sarchive()
spark = sarchive.spark

# Create Table
schema = StructType(
    [
        StructField("id", StringType()),
        StructField("first_name", StringType()),
        StructField("middle_name", StringType()),
        StructField("last_name", StringType()),
        StructField("dob", StringType()),
        StructField("gender", StringType()),
        StructField("salary", IntegerType())
    ]
)

TABLE_NAME = "s3a://sarchive/historical_db/demo_table"

# Check if it exists. If not create the empty table
# This is only important if you want to define a specific
# schema for later update. If that does not matter, this step can
# be skipped.
try:
    DeltaTable.forPath(spark, TABLE_NAME)
except AnalysisException:
    # If the table does not exist yet create an empty one
    empty_rdd = spark.sparkContext.emptyRDD()
    (
        spark.createDataFrame(empty_rdd, schema)
        .write.format("delta")
        .mode("overwrite")
        .save(TABLE_NAME)
    )

# Create Data Frame
data = [("1", "James", "", "Smith", "36636", "M", 60000),
        ("2", "Michael", "Rose", "", "40288", "M", 70000),
        ("3", "Robert", "", "Williams", "42114", "", 400000),
        ("4", "Maria", "Anne", "Jones", "39192", "F", 500000),
        ("5", "Jen", "Mary", "Brown", "", "F", 0)]

columns = ["id", "first_name", "middle_name", "last_name", "dob", "gender", "salary"]

# The following could also be done based on a Pandas Data Frame
# see https://sparkbyexamples.com/pyspark/convert-pandas-to-pyspark-dataframe/
pysparkDF = spark.createDataFrame(data=data, schema=columns)

# Upsert to table we created before (or which already exists)
existing_df = DeltaTable.forPath(spark, TABLE_NAME)

(
    existing_df.alias("existing")
    .merge(pysparkDF.alias("updates"), "existing.id = updates.id")
    .whenMatchedUpdateAll()
    .whenNotMatchedInsertAll()
    .execute()
)

# Re-Read data from SARchive
cases_sdf = spark.read.format("delta").load(TABLE_NAME)
cases_sdf.printSchema()
cases_sdf.show()

# Convert Cases to Pandas
cases_pdf = cases_sdf.toPandas()
print(cases_pdf)

# sarchive_python

Project demonstrating direct access to SARchive's Delta Lake and PostgreSQL from a Python program

## Getting started

### Python Environment

Create a virtual environment and install the requirements
```shell
python3 -m venv .venv
source .venv/bin/activate
pip3 install --upgrade pip setuptools wheel
pip install -r requirements.txt
```

### Additional Libraries for Spark

Spark needs some jars which are not automatically installed (located in the `libs` folder).

```shell
cp ./libs/*.jar .venv/lib/python3.8/site-packages/pyspark/jars
```

### Setup Delta Lake and Analytics DB
In our infrastructure we use [Delta Lake](https://delta.io/) storing data in an S3 Object Storage.
Our Analytics DB is a PostgreSQL DB with PostGIS installed.
On the development environment you can use two docker containers. One for MinIO (S3) and one for PostgreSQL.

Both can be created and started with `make dev`. If you use make, the next step (bucket creation) is already done.

After you created the containers, you have to navigate to the MinIO Web UI at [http://127.0.0.1:9000/minio/](http://127.0.0.1:9000/minio/).
There you have to create a bucket called `sarchive`. On you local machine you can use the logon `minioadmin/minioadmin`.

### Run the Examples

```shell
# Read a Data Frame from Spark
python3 -m sarchive_python.example.read_cases.py

# Create a Data Frame in Spark
python3 -m sarchive_python.example.create_data_frame.py
```